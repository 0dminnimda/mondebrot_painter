# Visualization of the Mandelbrot Set

![Mandelbrot Set](https://github.com/0dminnimda/mondebrot_painter/blob/master/2020/imgs/mandelbrot_set_2048_%23256f77_%23f7cf73.png?raw=true)

This repository serves more as a way to see my progress and "evolution" as a programmer/software engineer.  
Not to say that I am not interested in the topic, it is very close to my heart! ❤

![Corrupted_Mandelbrot Set](https://github.com/0dminnimda/mondebrot_painter/blob/master/2022/images/corrupted_xy.png?raw=true)

Look in the corresponding folder to find my implementation of the corresponding year.

![Corrupted_Mandelbrot Set](https://github.com/0dminnimda/mondebrot_painter/blob/master/2019/mon_img_8.png?raw=true)
